using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadName : MonoBehaviour
{
    public GameObject SaveName;
    public GameObject NewName;

    public void Awake()
    {
        NewName = GameObject.Find("Text (Legacy)");
        SaveName = GameObject.Find("FemaleName");

    }
    // Start is called before the first frame update
    void Start()
    {
        NewName.GetComponent<Text>().text = SaveName.GetComponent<Text>().text;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
