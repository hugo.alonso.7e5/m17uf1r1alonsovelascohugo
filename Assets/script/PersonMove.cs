using System.Collections;
using UnityEngine;

public class PersonMove : MonoBehaviour
{
    //1. Declaración de variables
    private float velocidad = 0.8f; //Velocidad del jugador

    private float salto = 4f;
    private bool canJump;
    private Rigidbody2D rb2d;
    private SpriteRenderer spRd;
    private Animator _animator;

    private void Start()
    {
        //2. Capturo y asocio los componentes Rigidbody2D y Sprite Renderer del Jugador
        velocidad = 4f;
        rb2d = GetComponent<Rigidbody2D>();
        spRd = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        //3. Movimiento horizontal
        float movimientoH = Input.GetAxisRaw("Horizontal");
        transform.position = new Vector2(movimientoH * velocidad*Time.deltaTime + transform.position.x, transform.position.y);
        _animator.SetBool("moverse", movimientoH != 0);
        //4. Sentido horizontal (para girar el render del jugador)
        if (movimientoH > 0)
        {
            spRd.flipX = false;
        }
        else if (movimientoH < 0)
        {
            spRd.flipX = true;
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && canJump)
        {
            canJump = false;
            Jump();
            
        }
        //_animator.SetBool("jumping", false);
    }

    public void Jump()
    {
        //_animator.SetBool("jumping", true);
        canJump = false;
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * salto, ForceMode2D.Impulse);
        _animator.SetBool("jumping", true);

    }
    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.collider.CompareTag("Suelo")) canJump = true;
    }
    public void acabarSalto()
    {
        _animator.SetBool("jumping", false);
    }
}