using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spwner : MonoBehaviour
{
    private GameObject fuegoBola;
    public Vector2 movement;

    // Start is called before the first frame update
    private void Start()
    {
        fuegoBola = GameObject.Find("fuegobola");
        InvokeRepeating("move", 0.001f, 0.4f);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void move()
    {
        movement = new Vector2(Random.Range(-9.0f, 9.0f), Random.Range(4.0f, 7.0f));
        Instantiate(fuegoBola, movement, Quaternion.identity);
        transform.position = movement;
    }
}