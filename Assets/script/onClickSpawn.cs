using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onClickSpawn : MonoBehaviour
{
    private GameObject fuegoBola;
    private Camera cam;
    private Vector2 mousePos = new Vector2();
    

    // Start is called before the first frame update
    private void Start()
    {
        
        cam = Camera.main;
        fuegoBola = GameObject.Find("fuegobola");
        
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Invoke("move", 0.001f);
        }
        Vector3 mousePos = Input.mousePosition;
        Debug.Log(Input.mousePosition.x + " " + Input.mousePosition.y);
    }

    private void move()
    {
        var point = new Vector2();
        point = cam.ScreenToWorldPoint(Input.mousePosition);
        point[1] = 5;
        Instantiate(fuegoBola, point, Quaternion.identity);
        transform.position = point;
    }
}