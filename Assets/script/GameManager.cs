using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private GameObject Field;
    private GameObject Female;
    private GameObject Name;
    private InputField InputField;
    private string text;
    private Text nombre;
    public void ActivarB1()
    {

        Name = GameObject.Find("FemaleName");

        Field = GameObject.Find("NewName");
        InputField = Field.GetComponent<InputField>();
        nombre = Name.GetComponent<Text>();

        nombre.text = InputField.text;

        Debug.Log(nombre.text);

        DontDestroyOnLoad(GameObject.Find("FemaleName"));

        SceneManager.LoadScene("joc1");
    }
    public void ActivarB2()
    {

        Name = GameObject.Find("FemaleName");

        Field = GameObject.Find("NewName");
        InputField = Field.GetComponent<InputField>();
        nombre = Name.GetComponent<Text>();

        nombre.text = InputField.text;

        Debug.Log(nombre.text);

        DontDestroyOnLoad(GameObject.Find("FemaleName"));

        SceneManager.LoadScene("joc2");
    }
}
