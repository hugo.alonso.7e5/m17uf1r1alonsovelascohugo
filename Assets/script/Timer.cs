using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public float targetTime = 60f;


    private void Update()
    {

        targetTime -= Time.deltaTime;
        gameObject.GetComponent<Text>().text = targetTime.ToString("N0");

        if (targetTime <= 0.0f)
        {
            timerEnded();
        }

    }

    void timerEnded()
    {
        SceneManager.LoadScene("Joc2Win");
    }
}
