
using UnityEngine;


public class SoloMove : MonoBehaviour
{
    enum direcciones { stop, derecha, izquierda }

    private direcciones direccion;
    public Vector3 pop;
    public float velocidad;


    void Start()
    {
        velocidad = 0.3f;
        direccion = direcciones.derecha;
        pop = new Vector3(velocidad * Time.deltaTime, 0f, 0f);
    }


    void Update()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        if (direccion == direcciones.izquierda)
        {
            transform.position -= pop;
            if (transform.position.x < -6.5f)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
                direccion = direcciones.derecha;


            }
        }
        else if (direccion == direcciones.derecha)
        {
            transform.position += pop;
            if (transform.position.x > 6.5f)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
                direccion = direcciones.izquierda;
            }
        }
    }
}
