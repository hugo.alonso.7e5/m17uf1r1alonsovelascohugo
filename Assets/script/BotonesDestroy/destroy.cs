using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroy : MonoBehaviour
{
    bool pep = false;
    private Animator _anim;
    void Start()
    {
        _anim = GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        pep = true;
        this.GetComponent<Rigidbody2D>().gravityScale = 0;
        this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<CapsuleCollider2D>().enabled = false;
        _anim.SetBool("colisionado", pep);
        
        
    }

    private void destruir()
    {
        Destroy(gameObject);
    }
}